import React, {useState} from 'react';
import './App.css';
import SeparatorTextComponent from "./shared/separator_text/SeparatorTextComponent";
import SingleInfoComponent from './shared/single_info/SingleInfoComponent';
import UsersComponent from "./features/users/Users";
interface UserModel{
  id: number;
  name: string;
  surname: string;
  email: string;
  address: string;
  hours: number;
  hourlyWage: string;
  salary: string;
  company: string;
  phone: string;
  city: string;
}
const users: UserModel[] = [
  {
    id: 1,
    name: 'Massimiliano',
    surname: 'Salerno',
    email: 'imafa82@gmail.com',
    address: 'Via Mediana 117, Rocca Priora(RM)',
    hours: 200,
    hourlyWage: '10 €',
    salary: '2000 €',
    company: 'Azienda del monte',
    phone: '06-5654121456',
    city: 'Roma'
  },
  {
    id: 2,
    name: 'Ilaria',
    surname: 'Badoni',
    email: 'ila@gmail.com',
    address: 'Via Cristoforo Colombo 117, Roma(RM)',
    hours: 50,
    hourlyWage: '10  €',
    salary: '500 €',
    company: 'Azienda del monte',
    phone: '06-5654121456',
    city: 'Roma'
  },
  {
    id: 3,
    name: 'Ted',
    surname: 'Carter',
    email: 'ted.carter@gmail.com',
    address: 'Via Libero Leonardi 117, Roma(RM)',
    hours: 30,
    hourlyWage: '10  €',
    salary: '300 €',
    company: 'Azienda del monte',
    phone: '06-5654121456',
    city: 'Roma'
  }
]

function App() {

  return (
    <div className="container">
      <UsersComponent></UsersComponent>
    </div>
  );
}

export default App;
