import React, {useEffect, useState} from "react";
import http from "../../shared/utils/http";
import UserViewComponent from "./components/UserView";
import {UserResponseModel} from "./model/UserModel";
const UsersComponent: React.FC = () => {
    const [users, setUsers] = useState<UserResponseModel[]>([]);
    useEffect(() => {
        http.get<UserResponseModel[]>(`users`).then(res => {
                setUsers(res);
            })
    }, [])

    return (
        <div>
            {
                users.length ? <UserViewComponent users={users} /> : <div>Caricamento Dati in corso</div>
            }
        </div>
    )
}

export default UsersComponent;
