import {DataModel, UserResponseModel, UserViewModel} from "./model/UserModel";
import {objDeleteProperties} from "../../shared/utils/objUtils";



export default function (user: UserResponseModel): ({data: DataModel[], user: UserViewModel}){
    return {
        user: {
          ...user,
          ...user.address.geo,
          ...objDeleteProperties(user.address, ['geo']),
          address: `${user.address.street}, ${user.address.suite}`,
        },
        data: [
        {
            title: 'Dati Utente',
            ico: 'user-circle-o',
            info: [
                {
                    label: 'Nome',
                    ico: 'user',
                    field: 'name'
                },
                {
                    label: 'Email',
                    ico: 'envelope-o',
                    field: 'email'
                },
                {
                    label: 'Telefono',
                    ico: 'phone',
                    field: 'phone'
                },
                {
                    label: 'Sito Web',
                    ico: 'internet',
                    field: 'website'
                },
            ]
        },
        {
            title: 'Indirizzo',
            ico: 'location-arrow',
            info: [
                {
                    label: 'Via',
                    ico: 'phone',
                    field: 'street'
                },
                {
                    label: 'Numero',
                    ico: 'phone',
                    field: 'suite'
                },
                {
                    label: 'Città',
                    ico: 'city',
                    field: 'city'
                },
                {
                    label: 'CAP',
                    ico: 'city',
                    field: 'zipcode'
                },
                {
                    label: 'Latitudine',
                    ico: 'lat',
                    field: 'lat'
                },
                {
                    label: 'Longitudine',
                    ico: 'lng',
                    field: 'lng'
                },
            ]
        }
    ]
    }
}
