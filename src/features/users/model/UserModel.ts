export interface AddressInfoBase{
    street: string;
    city: string;
    suite: string;
    zipcode: string;
}

export interface GeoInfo{
    lat: string;
    lng: string;
}

export interface AddressInfo extends AddressInfoBase{
    geo: GeoInfo
}
export interface AddressInfoView extends AddressInfoBase, GeoInfo{

}

export interface UserResponseModel{
    email: string;
    id: number;
    name: string;
    phone: string;
    username: string;
    website: string;
    address: AddressInfo
}
export interface UserViewModel extends AddressInfoView{
    email: string;
    id: number;
    name: string;
    phone: string;
    username: string;
    website: string;
    address: string;

}

export interface DataModel{
    title: string;
    ico: string;
    info: {
        label: string;
        ico: string;
        field: keyof UserViewModel;
    }[]
}
