import React, {useEffect, useState} from "react";
import SeparatorTextComponent from "../../../shared/separator_text/SeparatorTextComponent";
import SingleInfoComponent from "../../../shared/single_info/SingleInfoComponent";
import useUser from "../useUser";
import {UserResponseModel} from "../model/UserModel";
import TitleH1 from "../../../shared/titles/TitleH1";
import PrintView from "./PrintView";
const UserComponent: React.FC<{selectedUser: UserResponseModel}> = ({selectedUser}) => {
    const {data, user} = useUser(selectedUser);

    return (
        <>
            <TitleH1 text={user.name}/>
            <PrintView data={data} obj={user} />
        
        </>
    )
}

export default UserComponent;
