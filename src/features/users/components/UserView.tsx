import React, { useState} from "react";
import UserComponent from "./User";
import {UserResponseModel} from "../model/UserModel";
const UserViewComponent: React.FC<{users: UserResponseModel[]}> = ({users}) => {
    const [indexUser, setIndexUser]: any = useState(0);
    const selectedUser: any = users[indexUser];

    const decrementIndex = () => {
        setIndexUser(indexUser -1)
    }
    const incrementIndex = () => {
        setIndexUser(indexUser +1)
    }
    const backDisabled = indexUser < 1;
    const nextDisabled = indexUser >= (users.length -1);
    return (
         <div>
             <UserComponent selectedUser={selectedUser}/>
             <div className="mt-3 text-center">
                 <button className="btn btn-primary m-3" disabled={backDisabled} onClick={decrementIndex}>Indietro</button>
                 <button className="btn btn-primary m-3" disabled={nextDisabled} onClick={incrementIndex}>Prossimo</button>
             </div>
         </div>
    )
}

export default UserViewComponent;
