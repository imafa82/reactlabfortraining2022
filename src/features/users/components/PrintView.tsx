import React, {useEffect, useState} from "react";
import SeparatorTextComponent from "../../../shared/separator_text/SeparatorTextComponent";
import SingleInfoComponent from "../../../shared/single_info/SingleInfoComponent";
import { DataModel } from "../model/UserModel";

const PrintView: React.FC<{data: DataModel[], obj: any}> = ({data, obj}) => {

    return (
        <>
        
            {data.map((ele, index) => <div key={index}>
                <SeparatorTextComponent text={ele.title} icon={ele.ico} />
                <div className="row">
                    {ele.info.map((inf) => <div className="col-4" key={inf.field}>
                        <SingleInfoComponent icon={inf.ico} label={inf.label} text={obj[inf.field]?.toString()}/>
                    </div>)}
                </div>
            </div>)}
        </>
    )
}

export default PrintView;
