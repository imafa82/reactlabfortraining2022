import React from 'react';
interface SeparatorTextModel{
    text: string;
    icon: string;
}

function SeparatorTextComponent({icon, text}: SeparatorTextModel) {
  return (
      <div className="separator-text-component">
          <div className="actionIcon">
              <p><i className={'fa fa-'+icon} /> {text}</p>
          </div>
      </div>
  );
}

export default SeparatorTextComponent;
