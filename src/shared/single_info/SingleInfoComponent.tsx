import React from 'react';
interface SingleInfoModel{
    text?: string;
    label?: string;
    icon?: string
}

function SingleInfoComponent({icon, text, label}: SingleInfoModel) {
  return (
      <div className="single-info-component">
          {label && <p className="labelSingleInfo"><em>{label}</em></p>}
          {text && <p className="textSingleInfo"><span>{text}</span></p>}
          {icon && <i className={'fa fa-'+icon} />}
      </div>
  );
}

export default SingleInfoComponent;
