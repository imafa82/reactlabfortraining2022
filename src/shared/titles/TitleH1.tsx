import React from 'react';


function TitleH1({text}: {text: string}) {
  return (
    <h1 className="text-center">{text}</h1>
  );
}

export default TitleH1;
